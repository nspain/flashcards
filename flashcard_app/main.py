import gui
import sys
import tkinter as tk
import csv

def main():
    window = tk.Tk()
    word_dict = {}
    with open("cards.csv") as cards:
        word_dict = {row["spanish"]: row["english"] for row in
                csv.DictReader(cards)}

    window.geometry("500x500")
    root = gui.FlashCards(window, word_dict)
    window.mainloop()

if __name__ == "__main__":
    sys.exit(main())
