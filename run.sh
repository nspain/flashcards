#!/usr/bin/env bash4

PY="$(which python3)"

# Don't show STDOUT but send STDERR to error.log, run app in background
"$PY" flashcard_app/main.py > /dev/null 2> errors.log &

