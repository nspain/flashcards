#!/usr/bin/env python3

"""
Concat all CSV files in the current directory, removing headers, into all.csv.
This is step one for creating the quiz game using all the flashcard
definitions.
"""

import sys
import os

# UTF-8 encodings of the accented letters used in spanish
A = "\u00E1"
E = "\u00E9"
I = "\u00ED"
O = "\u00F3"
U = "\u00FA"
N = "\u00F1"

DIR = "csv_files/"

def make_cards(csvfiles):
    """
    Concatenate the files in CSVFILES, removing headers and replacing the LaTeX
    style accenting for UTF-8.
    """
    with open("cards.csv", "w") as all_files:
        all_files.write("spanish,english\n")
        for csvfile in csvfiles:
            utf8_lines = []
            with open(DIR+csvfile) as f:
                for line in f.readlines():
                    utf8_lines.append(ltx_to_utf8(line))
            # Write all lines from csvfile to all_files, except header
            all_files.write("".join(utf8_lines[1:]))

def ltx_to_utf8(string):
    """
    Replace LaTeX style accenting in string for UTF-8 encoding.
    For example:
        fot\'{o}grafo --> fot\u00F3grafo
    """
    string = string.replace(r"\'{a}",A)
    string = string.replace(r"\'{e}",E)
    string = string.replace(r"\'{i}",I)
    string = string.replace(r"\'{o}",O)
    string = string.replace(r"\'{u}",U)
    string = string.replace(r"\~{n}",N)
    return string

if __name__ == "__main__":
    files = [f for f in list(os.walk(DIR))[0][2] if f.split(".")[-1] == "csv"]
    sys.exit(make_cards(files))
