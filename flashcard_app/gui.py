import random
import tkinter as tk

# TODO:
#   - Add ability to do only one card category
#       - Tick boxes?
#   - Add ability to do english or spanish flashcards
#   - Add button to show answer, don't imediately show on wrong entry
#   - Arrange buttons more nicely:
#       Below is a possible scetch of the layout:
#   - Give stats on exit:
#       - Cards done, card correct, cards incorrect
#       - Suggest improvement subjects (if all)
#   - If o is chosen to replace @, swap el/la for el (same for a, except la)
#   - For words with multiple meainings (split by slashes) allow for any combo
#
#    Main menu                   Quit
#                   WORD
#                 RESPONSE
#             _________________
#             |               |     Next
#             -----------------
#                  Check
#                   Show
#
# Main menu -- Button, goes to list of possible cards decks, with option of all
#        - This is the page that we open the app to
# Quit -- Button, quit app
# Word -- Label, word to be answered
# Response -- Label, response to anwser, initially instructions on what to do
#               then whether or not the answer is correct
# Check -- Button, check if answer in entry box is correct
# Next -- Button, go to next word
# Show -- Button, show the answer to the current word


class FlashCards():
    def __init__(self, window, word_dict):
        self.word_dict = word_dict
        # Response to answer; Correct or Incorrect
        self.response_lbl_txt = tk.StringVar()

        # Word to be translated to English, in a format usable by Label
        self.word_lbl_txt = tk.StringVar()

        self.span_words = list(self.word_dict.keys())

        # Choose words to be guessed
        self.word = random.choice(self.span_words)

        # Instruction
        self.response_lbl_txt.set("Qu\u00E9 significa en {}?".format("ingleis"))

        self.word_lbl_txt.set(self.word.replace("@", random.choice(["o", "a"])))

        window.bind("<Return>", self.on_return_key)

        # Labels
        self.word_lbl = tk.Label(window, textvariable=self.word_lbl_txt)
        self.response_lbl= tk.Label(window, textvariable=self.response_lbl_txt)

        # Entry points
        self.entry_ent = tk.Entry(window)

        # Buttons
        self.check_btn = tk.Button(window, text="Check", command=self.check)
        self.next_btn = tk.Button(window, text="Next", command=self.next)
        self.quit_btn = tk.Button(window, text="Quit", command=quit)

        # Packing objects -> Making visable in frame
        self.word_lbl.pack()
        self.response_lbl.pack()
        self.entry_ent.pack()
        self.check_btn.pack()
        self.next_btn.pack()
        self.quit_btn.pack()

    def check(self):
        """
        Check that entry is equal to corresponding English of word to be
        guessed.
        """

        # Convert to actual and expected answers to lower case and remove padding
        actual = self.entry_ent.get().lower().strip()
        expected = self.word_dict[self.word].lower().strip()
        if expected == actual:
            self.response_lbl_txt.set("Correcto! Apriete 'Next' para una otra palabra!")
        else:
            self.response_lbl_txt.set("Incorrect! La respuesta es '{}'".format(
                self.word_dict[self.word]))

    def next(self):
        """
        Set word and word_lbl_txt with a random word from the Spanish word
        list.
        """
        self.word = random.choice(self.span_words)
        self.word_lbl_txt.set(self.word.replace("@", random.choice(["o", "a"])))
        self.response_lbl_txt.set("Qu\u00E9 significa en {}?".format("ingleis"))
        self.entry_ent.delete(0, tk.END)

    def on_return_key(self, *args):
        """ Call check function when return key is pressed. """
        self.check()



