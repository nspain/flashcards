#!/usr/bin/env python3
# Author: Nicholas Spain
# Fri  3 Feb 2017 12:12:28 AEDT

"""
Produce flash cards from 'cards.csv' using the *flashcards* document style.
Each flash card is of the form:
    \begin{flashcard}[<type>]{<spanish>}
        <english>
    \end{flashcard}
This script creates the file flashcards.ltx and compiles it.
"""

import csv
import sys
import subprocess
import os

CSV_FILES = "./csv_files/"

def main():
    fmt = []
    for fh in [f for f in list(os.walk(CSV_FILES))[0][2] if f.split(".")[-1]=="csv"]:
        with open(CSV_FILES+fh) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                fmt.append("\\begin{flashcard}["+fh.split(".")[0]+"]{"+\
                        row["spanish"]+"}\n"+row["english"]+"\n\\end{flashcard}\n")
    with open("flashcards.ltx", "w") as f:
        f.write("\\documentclass[avery5371, grid]{flashcards}\n")
        f.write("\\usepackage[latin1]{inputenc}\n")
        f.write("\\usepackage{amsfonts}\n")
        f.write("\\usepackage{amsmath}\n")
        f.write("\cardfrontstyle[\large\slshape]{headings}\n")
        f.write("\cardbackstyle{plain}\n")
        f.write("\\begin{document}\n")
        f.write("\n".join(fmt))
        f.write("\n\\end{document}\n")




if __name__ == "__main__":
    sys.exit(main())
