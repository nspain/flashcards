PYTH = $(shell which python3)
LTX = $(shell which pdflatex)
MKCARD = makeflashcards.py
CONCATCSV = flashcard_app/concat.py
INLTX = flashcards.ltx

build:
	$(PYTH) $(MKCARD) && $(LTX) $(INLTX) && $(PYTH) $(CONCATCSV)

clean:
	rm -rf *.aux *.pdf *.log
